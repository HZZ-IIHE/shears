import json
import os
import subprocess

import yaml


class Dataset:
    """Class representing a single dataset.

    Following data members are defined:
        name:  Full name of the dataset as appears in DAS.
        short_name:  Shorter name that should still provide an
            unambiguous identification of the dataset within a campaign.
            Must meet the requirements for a valid file name; in
            particular, must not contain a slash.  It not given,
            defaults to the primary name of the dataset.
        selections:  List of selections to be applied.  Consists of
            pairs of strings identifying selection and subselection to
            be applied in pruner.
        crab_cfg_extension:  Additional parameters to be spliced into
            the default CRAB configuration.
        is_sim:  Shows whether this is simulation or real data.  Deduced
            automatically based on the full dataset name.
    """
    def __init__(self, name, selections, short_name=None,
                 crab_cfg_extension={}):
        self.name = name
        self.is_sim = name.endswith('SIM') or name.endswith('USER')
        self.selections = selections
        self.crab_cfg_extension = crab_cfg_extension

        if short_name:
            self.short_name = short_name
        else:
            self.short_name = self.primary_name


    def build_task_name(self, selection):
        """Build CRAB task name for this dataset.

        Arguments:
            selection:  A pair of strings representing selection and
                subselection to tbe applied in pruner.

        Return value:
            Name for the CRAB task.
        """
        return '{}_{}-{}'.format(self.short_name, selection[0], selection[1])


    @property
    def primary_name(self):
        """Return primary dataset name."""
        return self.name.split('/')[1]


class DatasetGroup:
    """Represents a group of datasets.

    Following data members are defined:
        name:  Text string uniquely identifying the group within a
            campaign.  Must meet the requirements for a valid file name;
            in particular, must not contain a slash.
        datasets:  Non-empty list of datasets included in the group.
            Each entry is of type Dataset.
        selections:  Non-empty list of event selections to be applied.
            Shared among all datasets in the group.
        is_sim:  Shows whether this is simulation or real data.  Only
            datasets of the same type can be combined in a group.
        crab_cfg_extension:  Additional parameters to be spliced into
            the default CRAB configuration.  Shared among all datasets
            in the group.
    """
    def __init__(self, config):
        if 'name' in config:
            self.name = config['name']
        else:
            # If the group name has not been provided, use the primary
            # name of the first dataset in place of it
            self.name = config['datasets'][0].split('/')[1]

        self.selections = config['selections']
        self.crab_cfg_extension = config.get('crab', {})

        for key in self.crab_cfg_extension:
            if key not in {
                'General', 'JobType', 'Data', 'Site', 'User', 'Debug'
            }:
                raise RuntimeError(
                    'Unknown key "{}" found in CRAB configuration extension '
                    'for dataset group "{}".'.format(key, self.name)
                )

        self.datasets = []

        for i, name in enumerate(config['datasets']):
            self.datasets.append(Dataset(
                name, self.selections,
                short_name='{}_p{}'.format(self.name, i),
                crab_cfg_extension=self.crab_cfg_extension
            ))

        self.is_sim = self.datasets[0].is_sim


class FilePath:
    """File path at IIHE storage element."""

    def __init__(self, lfn):
        """Initialize from an LFN ("/store/...")."""
        self.lfn = lfn

    def __str__(self):
        return self.lfn

    @property
    def local(self):
        """Return path on locally mounted file system."""
        return '/pnfs/iihe/cms' + self.lfn

    @property
    def srm(self):
        """Return path with SRM protocol prefix applied."""
        return ('srm://maite.iihe.ac.be:8443/srm/managerv2?SFN='
                '/pnfs/iihe/cms' + self.lfn)


class Config:
    """Interface to access master configuration."""

    def __init__(self, path):
        """Initialize from path to YAML configuration file."""
        with open(path) as f:
            self._config = yaml.safe_load(f)

        self.dataset_groups = [
            DatasetGroup(c) for c in self._config['samples']
        ]

        # Make sure that names of dataset groups are unique
        seen_names = set()
        for group in self.dataset_groups:
            if group.name in seen_names:
                raise RuntimeError('Multiple dataset groups have name '
                                   '"{}".'.format(group.name))
            seen_names.add(group.name)


    def check_selections(self, pruner_path):
        """Verify that all selections are supported by pruner.

        If any of the selections specified in the configuration file is
        not supported by the pruner, raise an exception.

        Arguments:
            pruner_path:  Path to pruner executable.

        Return value:
            None.
        """

        pruner_output = json.loads(subprocess.check_output([
            pruner_path, '--list-selections', '--json']))
        allowed_selections = {(p[0], p[1]) for p in pruner_output}

        for group in self.dataset_groups:
            if not group.selections:
                raise RuntimeError('No selections specified for dataset '
                                   'group "{}".'.format(group.name))

            for selection in group.selections:
                if len(selection) != 2:
                    raise RuntimeError(
                        'Wrong number of elements in selection ({}) for dataset '
                        'group "{}".'.format(
                            ', '.join('"{}"'.format(s) for s in selection),
                            group.name
                        )
                    )

                if (selection[0], selection[1]) not in allowed_selections:
                    raise RuntimeError(
                        'Unknown selection ("{}", "{}") specified for dataset '
                        'group "{}".'.format(
                            selection[0], selection[1], group.name)
                    )


    def data_lumi_mask(self):
        return self._config['data_lumi_mask']


    def datasets(self):
        """Provide access to ungrouped datasets.
        
        When multiple datasets are grouped together, provide them
        individually, repeating shared properties.
        """

        for group in self.dataset_groups:
            for dataset in group.datasets:
                yield dataset


    def ddf_dir(self):
        """Return directory for dataset definition files.

        Represented by a FilePath object.
        """
        return FilePath(os.path.join(self._config['storage'], 'DDF'))


    def stageout_dir(self):
        """Return directory to stage out output from Grid.
        
        Represented by a FilePath object.
        """
        return FilePath(os.path.join(self._config['storage'], 'stageout'))


    def pileup_dir(self):
      """Return directory for pileup profiles.

      Represented by a FilePath object.
      """
      return FilePath(os.path.join(self._config['storage'], 'pileup'))


    def year(self):
      return self._config['year']
