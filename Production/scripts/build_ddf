#!/usr/bin/env python

"""Creates dataset definition files."""

from __future__ import division, print_function
import argparse
from collections import namedtuple
import os
import re
import subprocess
import shutil
import sys
from uuid import uuid4

import ROOT
import yaml

from shears.Production.config import Config


WeightInfo = namedtuple(
    'WeightInfo', ['lhe_scale_present', 'ps_present']
)

def check_weights(files):
    """Check if generator weights of different types are present.

    Arguments:
        files:  Iterable with paths to NanoAOD files.

    Return value:
        An instance of WeightInfo.
    """

    lhe_scale_present = True
    ps_present = True
    all_empty = True

    # Loop over all files in case they come from different datasets with
    # different weight settings.  Mark the weights as present only if
    # they are found in all files.
    for path in files:
        input_file = ROOT.TFile(path)
        tree = input_file.Get('Events')
        if tree.GetEntries() == 0:
            continue

        tree.SetBranchStatus('*', False)
        for branch in ['nLHEScaleWeight', 'nPSWeight']:
            tree.SetBranchStatus(branch, True)

        # Only check the first event as the weight settings should be
        # the same for all events in a given dataset
        event = next(iter(tree))
        all_empty = False

        # In some cases these arrays contain single dummy entries.  For
        # this reason compare the lengths of the arrays against 1 and
        # not 0.
        if event.nLHEScaleWeight <= 1:
            lhe_scale_present = False
        if event.nPSWeight <= 1:
            ps_present = False

        input_file.Close()

    if all_empty:
        lhe_scale_present = False
        ps_present = False

    return WeightInfo(lhe_scale_present, ps_present)


def count_events_sim(files):
    """Count events and compute event weights in a set of files.

    Arguments:
        files:  Iterable with paths to NanoAOD files with added event
            counts.

    Return value:
        Tuple consisting of the number of all processed events, number
        of selected events, and mean generator-level event weight before
        the selection.
    """

    num_total = 0
    num_selected = 0
    sum_nominal_weight = 0.

    for path in files:
        input_file = ROOT.TFile(path)

        header_tree = input_file.Get('EventCounts')
        for entry in header_tree:
            num_total += entry.InEvtCount
            sum_nominal_weight += entry.InGeneratorWeightSum

        event_tree = input_file.Get('Events')
        num_selected += event_tree.GetEntries()

        input_file.Close()

    return num_total, num_selected, sum_nominal_weight / num_total


def count_events(files):
    """Compute total number of events in a set of NanoAOD files.

    Arguments:
        files:  Iterable with paths to NanoAOD files.

    Return value:
        Number of events.
    """

    num_selected = 0

    for path in files:
        input_file = ROOT.TFile(path)

        event_tree = input_file.Get('Events')
        num_selected += event_tree.GetEntries()

        input_file.Close()

    return num_selected


def find_files(dataset_group, selection, stageout_dir):
    """Find Grid output files for a group of datasets.

    Arguments:
        dataset_group:  config.DatasetGroup object that defines a group
            of datasets.
        selection:  Pair of selection and subselection labels that
            identifies the event selection applied.
        stageout_dir:  config.FilePath object that represents Grid
            stageout directory for the production campaign.
    """
    files = []

    for dataset in dataset_group.datasets:
        task_name = dataset.build_task_name(selection)
        src_dir = os.path.join(
            stageout_dir.local, dataset.primary_name, 'crab_' + task_name)

        timestamp_subdirs = sorted(os.listdir(src_dir))
        if len(timestamp_subdirs) > 1:
            print(
                'Found output for multiple tasks for dataset "{}". Will only '
                'use the latest one ("{}").'.format(
                    dataset.name, timestamp_subdirs[-1]),
                file=sys.stderr
            )
        timestamp_subdir = timestamp_subdirs[-1]

        for counter_subdir in os.listdir(
            os.path.join(src_dir, timestamp_subdir)
        ):
            for filename in os.listdir(os.path.join(
                src_dir, timestamp_subdir, counter_subdir
            )):
                if (not filename.startswith(task_name) or
                    not filename.endswith('.root')):
                    continue

                files.append(os.path.join(
                    src_dir, timestamp_subdir, counter_subdir, filename
                ))

    return files



if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('config',
                            help='Configuration file with a list of datasets.')
    args = arg_parser.parse_args()

    config = Config(args.config)

    ddf_target_dir = config.ddf_dir().srm
    subprocess.check_call(['gfal-mkdir', '-p', ddf_target_dir])
    ddf_tmp_dir = 'ddf_' + uuid4().hex
    os.mkdir(ddf_tmp_dir)
    known_subdirs = set()

    for group in config.dataset_groups:
        for selection in group.selections:
            subdir = selection[0]
            if subdir not in known_subdirs:
                subprocess.check_call([
                    'gfal-mkdir', '-p', os.path.join(ddf_target_dir, subdir)
                ])
                os.mkdir(os.path.join(ddf_tmp_dir, subdir))
                known_subdirs.add(subdir)

            files = find_files(group, selection, config.stageout_dir())

            ddf = {
                'stem': group.name,
                'files': files
            }

            if group.is_sim:
                num_total, num_selected, mean_weight = count_events_sim(files)
                weight_info = check_weights(files)
                ddf.update({
                    'num_events': num_total,
                    'num_selected_events': num_selected,
                    'mean_weight': mean_weight,
                    'weights': {
                        'lhe_scale': weight_info.lhe_scale_present,
                        'ps_scale': weight_info.ps_present
                    }
                })
            else:
                ddf.update({
                    'num_selected_events': count_events(files)
                })

            ddf_tmp_path = os.path.join(ddf_tmp_dir, subdir, group.name + '.yaml')
            with open(ddf_tmp_path, 'w') as f:
                yaml.safe_dump(ddf, f, default_flow_style=False)
            subprocess.check_call([
                'gfal-copy', ddf_tmp_path, os.path.join(ddf_target_dir, subdir)
            ])

    shutil.rmtree(ddf_tmp_dir)

