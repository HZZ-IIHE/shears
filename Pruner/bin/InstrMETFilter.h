#ifndef SHEARS_PRUNER_BIN_INSTRMETFILTER_H_
#define SHEARS_PRUNER_BIN_INSTRMETFILTER_H_

#include <optional>
#include <string>
#include <vector>

#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

#include "FilterBase.h"
#include "TriggerFilter.h"


/// Implements loose event selection for data-driven ptmiss
class InstrMETFilter : public FilterBase {
 public:
  InstrMETFilter(std::string const &triggerConfigPath,
      std::string const &year);

  bool Filter() const override;
  void Initialize(TTreeReader &reader) override;

  /**
   * \brief Return list of supported subselections
   *
   * This method is provided for the sake of uniformity with DileptonFilter.
   */
  static std::vector<std::string> SubSelections() {
    return {"SinglePhoton"};
  }

 private:
  /**
   * \brief Auxiliary structure to simplify delayed initialization of branches
   * read from source tree
   */
  struct Source {
    Source(TTreeReader &reader, std::string const &idBranchName);

    TTreeReaderValue<UInt_t> photonNum;
    TTreeReaderArray<Float_t> photonPt;
    TTreeReaderArray<Int_t> photonCutBased;
  };

  // Identify whether a given Photon ID is tight or not, according to EGamma POG
  // recommendation: https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedPhotonIde-
  // ntificationRun2?rev=52#Cut_Based_Photon_ID_for_Run_2
  // Photon cut IDs definitions are (0:fail, 1::loose, 2:medium, 3:tight)
  static bool IsTightPhoton(int photonID);

  TriggerFilter triggerFilter_;
  mutable std::optional<Source> src_;
  std::string idBranchName_;
};

#endif  // SHEARS_PRUNER_BIN_INSTRMETFILTER_H_

