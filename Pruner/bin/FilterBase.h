#ifndef SHEARS_PRUNER_BIN_FILTERBASE_H_
#define SHEARS_PRUNER_BIN_FILTERBASE_H_

#include <TTreeReader.h>


/// Interface for a filter used in Pruner
class FilterBase {
 public:
  virtual ~FilterBase() = default;

  /// Decide whether the current event should be kept
  virtual bool Filter() const = 0;

  /// Initialize for given reader of source tree
  virtual void Initialize(TTreeReader &reader) = 0;
};

#endif  // SHEARS_PRUNER_BIN_FILTERBASE_H_

