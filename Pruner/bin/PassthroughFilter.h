#ifndef SHEARS_PRUNER_BIN_PASSTHROUGHFILTER_H_
#define SHEARS_PRUNER_BIN_PASSTHROUGHFILTER_H_

#include <string>
#include <vector>

#include "FilterBase.h"


/// Trivial filter that accepts every event
class PassthroughFilter : public FilterBase {
 public:
  PassthroughFilter() = default;

  bool Filter() const override {
    return true;
  }

  void Initialize(TTreeReader &) override {}

  static std::vector<std::string> SubSelections() {
    return {""};
  }
};

#endif  // SHEARS_PRUNER_BIN_PASSTHROUGHFILTER_H_
